﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;

namespace Arma3Launcher
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        
        

        private void cmdUpdate_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void cmdclose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void cmdChangeSteampath_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog

            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            dlg.DefaultExt = ".exe";
            dlg.Filter = "arma3.exe|*.exe";

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            //Get the selected file name and display in Textbox
            if (result == true)
            {
                //Open document
                string steampath = dlg.FileName;
                
                //Steampath Validator
                if (steampath.EndsWith("arma3.exe"))
                {
                    MessageBox.Show("Arma 3 wurde gefunden!");
                }
                else
                {
                    MessageBox.Show("Wähle arma3.exe");
                }
             
                lblSteamPath.Content = steampath;
            }
        }

        private void cmdJoinServer_Click(object sender, RoutedEventArgs e)
        {
            //Check if file exists
            string curFile = @lblSteamPath.Content.ToString();
            if (File.Exists(curFile))
            {
                //Check if file endswith arma3.exe
                if ((curFile.EndsWith("arma3.exe")))
                {
                    /////////////////////////////////
                    //     Laucher settings        //
                    /////////////////////////////////

                    //Server IP
                    string launchip = "5.16.195.105";
                    //Server Port
                    string launchport = "389";
                    //Server Password
                    string launchpasswod = "password";
                    //Mods to launch
                    string launchmod = "mod";
                    //Settings
                    bool launchnosplash = true;
                    bool launchskiptintro = true;

                    //Creating launcharguments
                    string launcharguments = "";
                    if (launchnosplash==true)
                    {
                        launcharguments += " -nosplash";
                    }
                    if (launchskiptintro == true)
                    {
                        launcharguments += " -skipIntro";
                    }

                    if (launchip != "")
                    {
                        launcharguments += " -connect=" + launchip;
                    }

                    if (launchport != "")
                    {
                        launcharguments += " -port=" + launchport;
                    }

                    if (launchpasswod != "")
                    {
                        launcharguments += " -password=" + launchpasswod;
                    }

                    if (launchmod != "")
                    {
                        launcharguments += " -mod=" + launchmod;
                    }


                    // Prepare the process to run
                    ProcessStartInfo start = new ProcessStartInfo();
                    // Enter in the command line arguments, everything you would enter after the executable name itself
                    start.Arguments = launcharguments;
                    // Enter the executable to run, including the complete path
                    start.FileName = lblSteamPath.Content.ToString();
                    // Do you want to show a console window?
                    start.WindowStyle = ProcessWindowStyle.Hidden;
                    start.CreateNoWindow = true;

                    // Run the external process & wait for it to finish
                    using (Process proc = Process.Start(start))
                    {
                        proc.WaitForExit();

                        // Retrieve the app's exit code
                        //exitCode = proc.ExitCode;
                    }
                }
                else
                {
                    MessageBox.Show("Datei endet nicht auf arma.exe");
                }
            }
            else
            {
                MessageBox.Show("Pfad stimmt nicht!");
            }
                
           
        }

        private void Updating()
        {


        }
    }
}
